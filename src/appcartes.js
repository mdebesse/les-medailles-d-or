// Acquisition des div
let elementsIds = ['boutonAjoutCartes','listeCartes','modifCartes','conteneurCartes','champQuestion','champProp1','champProp2','champProp3','champProp4','champBoutons','modifCartesTitre','checkProp1','checkProp2','checkProp3','checkProp4'];
let elements = {};
elementsIds.forEach(id => {
	elements[id] = document.getElementById(id);
});
let maQuestion = ['','','','','',''];

// Mise en oeuvre
listerCartes();

// Fonctions
function cacherTout() {
	let elacacher = [boutonAjoutCartes, listeCartes, modifCartes];
	elacacher.forEach(element => {element.style.display = 'none';}); 
}

function listerCartes() {
	cacherTout();
	listeCartes.style.display = 'block';
	boutonAjoutCartes.style.display = 'block';
	let contenuHTML = '';
	questions.forEach((el, i) => {
		if (el[1] == 0) {
			contenuHTML += '<div class="boite centrer"><h4>'+ el[0] +'</h4><div class="prop1">'+ el[2] +'</div><div class="prop2">'+ el[3] +'</div><div class="prop3">'+ el[4] +'</div><div class="prop4">'+ el[5] +'</div><div class="petitBouton main" onclick="modifierQuestion('+i+')">✏️ Modifier la carte</div></div>';
		}
		if (el[1] == 1) {
			contenuHTML += '<div class="boite centrer"><h4>'+ el[0] +'</h4><div class="prop1 bRep">'+ el[2] +'</div><div class="prop2">'+ el[3] +'</div><div class="prop3">'+ el[4] +'</div><div class="prop4">'+ el[5] +'</div><div class="petitBouton main" onclick="modifierQuestion('+i+')">✏️ Modifier la carte</div></div>';
		}
		if (el[1] == 2) {
			contenuHTML += '<div class="boite centrer"><h4>'+ el[0] +'</h4><div class="prop1">'+ el[2] +'</div><div class="prop2 bRep">'+ el[3] +'</div><div class="prop3">'+ el[4] +'</div><div class="prop4">'+ el[5] +'</div><div class="petitBouton main" onclick="modifierQuestion('+i+')">✏️ Modifier la carte</div></div>';
		}
		if (el[1] == 3) {
			contenuHTML += '<div class="boite centrer"><h4>'+ el[0] +'</h4><div class="prop1">'+ el[2] +'</div><div class="prop2">'+ el[3] +'</div><div class="prop3 bRep">'+ el[4] +'</div><div class="prop4">'+ el[5] +'</div><div class="petitBouton main" onclick="modifierQuestion('+i+')">✏️ Modifier la carte</div></div>';
		}
		if (el[1] == 4) {
			contenuHTML += '<div class="boite centrer"><h4>'+ el[0] +'</h4><div class="prop1">'+ el[2] +'</div><div class="prop2">'+ el[3] +'</div><div class="prop3">'+ el[4] +'</div><div class="prop4 bRep">'+ el[5] +'</div><div class="petitBouton main" onclick="modifierQuestion('+i+')">✏️ Modifier la carte</div></div>';
		}
	});
	conteneurCartes.innerHTML = contenuHTML;
}

function modifierQuestion(id) {
	cacherTout();
	modifCartes.style.display = 'block';
	modifCartesTitre.innerHTML = 'Modifier la carte';
	maQuestion[0] = questions[id][0];
	maQuestion[1] = questions[id][1];
	maQuestion[2] = questions[id][2];
	maQuestion[3] = questions[id][3];
	maQuestion[4] = questions[id][4];
	maQuestion[5] = questions[id][5];
	champQuestion.innerHTML = maQuestion[0];
	champProp1.innerHTML = maQuestion[2];
	champProp2.innerHTML = maQuestion[3];
	champProp3.innerHTML = maQuestion[4];
	champProp4.innerHTML = maQuestion[5];
	champBoutons.innerHTML = '<div class="petitBouton main" onclick="supprimerQuestion('+id+')">🗑️ Supprimer</div><div onclick="listerCartes()" class="petitBouton main">❌ Annuler</div><div class="petitBouton main" onclick="sauvegarderModif('+id+')">✅ Valider</div>';
	modifierQuestion2();
}

function modifierQuestion2() {
	checkProp1.innerHTML = checkProp2.innerHTML = checkProp3.innerHTML = checkProp4.innerHTML = '❌';
	if(maQuestion[1] == 1) {checkProp1.innerHTML = '✅';};
	if(maQuestion[1] == 2) {checkProp2.innerHTML = '✅';};
	if(maQuestion[1] == 3) {checkProp3.innerHTML = '✅';};
	if(maQuestion[1] == 4) {checkProp4.innerHTML = '✅';};
}

function sauvegarderModif(id) {
	removeTrailingBr(champQuestion);
	removeTrailingBr(champProp1);
	removeTrailingBr(champProp2);
	removeTrailingBr(champProp3);
	removeTrailingBr(champProp4);
	maQuestion[0] = champQuestion.innerHTML;
	maQuestion[2] = champProp1.innerHTML;
	maQuestion[3] = champProp2.innerHTML;
	maQuestion[4] = champProp3.innerHTML;
	maQuestion[5] = champProp4.innerHTML;
	questions[id] = maQuestion.slice();
	listerCartes();
}

function sauvegarderAjout() {
	removeTrailingBr(champQuestion);
	removeTrailingBr(champProp1);
	removeTrailingBr(champProp2);
	removeTrailingBr(champProp3);
	removeTrailingBr(champProp4);
	maQuestion[0] = champQuestion.innerHTML;
	maQuestion[2] = champProp1.innerHTML;
	maQuestion[3] = champProp2.innerHTML;
	maQuestion[4] = champProp3.innerHTML;
	maQuestion[5] = champProp4.innerHTML;
	let nvQuestion = maQuestion.slice();
	questions.push(nvQuestion);
	listerCartes();
}

function supprimerQuestion(id) {
	questions.splice(id, 1);
	listerCartes();
}

function reponse(id) {
	maQuestion[1] = id;
	modifierQuestion2();
}

function ajouterCarte() {
	cacherTout();
	modifCartes.style.display = 'block';
	modifCartesTitre.innerHTML = 'Modifier la carte';
	maQuestion[0] = 'Question';
	maQuestion[1] = 0;
	maQuestion[2] = 'Proposition 1';
	maQuestion[3] = 'Proposition 2';
	maQuestion[4] = 'Proposition 3';
	maQuestion[5] = 'Proposition 4';
	champQuestion.innerHTML = maQuestion[0];
	champProp1.innerHTML = maQuestion[2];
	champProp2.innerHTML = maQuestion[3];
	champProp3.innerHTML = maQuestion[4];
	champProp4.innerHTML = maQuestion[5];
	champBoutons.innerHTML = '<div onclick="listerCartes()" class="petitBouton main">❌ Annuler</div><div class="petitBouton main" onclick="sauvegarderAjout()">✅ Valider</div>';
	modifierQuestion2();
}

function sauvegarder() {
	let contenuSauvegarde = 'let questions = '+JSON.stringify(questions)+';';
	let blob = new Blob([contenuSauvegarde], {
		type: "application/javascript;charset=utf-8"
	});
	let url = URL.createObjectURL(blob);
	let a = document.createElement("a");
	a.href = url;
	a.download = "cartes.js";
	a.click();
	URL.revokeObjectURL(url);
}

function removeTrailingBr(element) {
    if (element.innerHTML.endsWith('<br>')) {
        element.innerHTML = element.innerHTML.replace(/<br>$/, '');
    }
}