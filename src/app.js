// Audio
let lecteur = document.getElementById('lecteur');
let audioIcon = document.getElementById('audio');
let audio = true;

audioIcon.addEventListener('click', function() {
	if (audio === true) {
		audioIcon.src = 'src/audio-off.svg';
		audio = false;
		lecteur.muted = true;
	} else {
		audioIcon.src = 'src/audio-on.svg';
		audio = true;
		lecteur.muted = false;
	}
});

// Drag & Drop
document.querySelectorAll(".pion").forEach(function(el){
  dragElement(el);
});

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "top-content")) {
    /* if present, the header is where you move the DIV from:*/
    document.getElementById(elmnt.id + "top-content").onmousedown = dragMouseDown;
    document.getElementById(elmnt.id + "top-content").ontouchstart = dragMouseDown;
  } else {
    /* otherwise, move the DIV from anywhere inside the DIV:*/
    elmnt.onmousedown = dragMouseDown;
    elmnt.ontouchstart = dragMouseDown;
  }

  function dragMouseDown(e) {
  	elmnt.style.position = 'absolute';
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX || e.touches[0].clientX;
    pos4 = e.clientY || e.touches[0].clientY;
    document.onmouseup = closeDragElement;
    document.ontouchend = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
    document.ontouchmove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - (e.clientX || e.touches[0].clientX);
    pos2 = pos4 - (e.clientY || e.touches[0].clientY);
    pos3 = e.clientX || e.touches[0].clientX;
    pos4 = e.clientY || e.touches[0].clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse/touch is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
    document.ontouchend = null;
    document.ontouchmove = null;
  }
}

// Dès
window.addEventListener('DOMContentLoaded', function() {
  var boutonDes = document.getElementById('boutonDes');
  var monDes = document.getElementById('monDes');

  boutonDes.addEventListener('click', function() {
      lecteur.pause();
      lecteur.src = "src/des.mp3";
      lecteur.currentTime = 0;
      lecteur.play();
    boutonDes.classList.add('clignoter');
    monDes.classList.add('clignoter');

    setTimeout(function() {
      var resultatDes = Math.floor(Math.random() * 6) + 1; // Génère un nombre aléatoire entre 1 et 6
      monDes.src = 'src/' + resultatDes + '.svg'; // Met à jour l'image du dé en fonction du résultat
      
      boutonDes.classList.remove('clignoter');
      monDes.classList.remove('clignoter');
    }, 1000); // Attend 2 secondes avant d'afficher le nombre final
  });
});

// Actions cartes
let sauvegarde = questions.slice(0);
let question;

// Récupérer les éléments HTML
const cartesBoite = document.getElementById('cartesBoite');
const maCarte = document.getElementById('maCarte');
const cache = document.getElementById('cache');
const fermer = document.getElementById('fermer');
const questionTexte = document.getElementById('question');
const propositions = [
  document.getElementById('prop1'),
  document.getElementById('prop2'),
  document.getElementById('prop3'),
  document.getElementById('prop4')
];

// Fonction pour afficher une question aléatoire dans #maCarte
function afficherQuestion() {
  // Vérifier s'il n'y a plus de cartes dans la pioche
  if (questions.length === 0) {
    // Remettre toutes les cartes dans la pioche
    questions = sauvegarde.slice(0);
  }

  // Générer un nombre aléatoire correspondant à l'index d'une question
  const indexQuestion = Math.floor(Math.random() * questions.length);
  question = questions[indexQuestion].slice(0);

  // Supprimer la question de la pioche
  questions.splice(indexQuestion, 1);

  // Afficher la question et les propositions dans #maCarte
  questionTexte.innerHTML = question[0];
  for (let i = 0; i < propositions.length; i++) {
    propositions[i].innerHTML = question[i + 2];
    propositions[i].style.backgroundColor = '';
    propositions[i].addEventListener('click', verifierReponse);
  }

  // Afficher la carte avec un effet de fondu
  maCarte.style.opacity = '0';
  maCarte.style.display = 'block';
  cache.style.opacity = '0';
  cache.style.display = 'block';

  // Lancer l'animation de fondu
  setTimeout(() => {
    maCarte.style.opacity = '1';
    cache.style.opacity = '0.5';
  }, 100);
}

// Fonction pour cacher la carte avec un effet de fondu
function cacherCarte() {
  maCarte.style.opacity = '1';
  cache.style.opacity = '0.5';

  // Lancer l'animation de fondu
  setTimeout(() => {
    maCarte.style.opacity = '0';
    maCarte.style.display = 'none';
    cache.style.opacity = '0';
    cache.style.display = 'none';
  }, 100);
}

// Fonction pour vérifier la réponse sélectionnée
function verifierReponse(event) {
  const reponseSelectionnee = event.target;
  const indexQuestion = Math.floor(Math.random() * questions.length);

  // Vérifier si la réponse est correcte
  if (reponseSelectionnee.textContent === question[parseInt(question[1]) + 1]) {
    reponseSelectionnee.style.backgroundColor = '#a9dfbf';
    lecteur.pause();
    lecteur.src = "src/oui.mp3";
    lecteur.currentTime = 0;
    lecteur.play();
  } else {
    // Trouver la bonne réponse et la mettre en vert
    for (let i = 0; i < propositions.length; i++) {
      if (propositions[i].textContent === question[parseInt(question[1]) + 1]) {
        propositions[i].style.backgroundColor = '#a9dfbf';
        break;
      }
    }
    reponseSelectionnee.style.backgroundColor = '#f5b7b1';
    lecteur.pause();
    lecteur.src = "src/non.mp3";
    lecteur.currentTime = 0;
    lecteur.play();
  }

  // Désactiver les clics sur les autres propositions
  for (let i = 0; i < propositions.length; i++) {
    propositions[i].removeEventListener('click', verifierReponse);
  }
}

// Ajouter un écouteur d'événement au clic sur #cartesBoite
cartesBoite.addEventListener('click', afficherQuestion);

// Ajouter un écouteur d'événement au clic sur #fermer
fermer.addEventListener('click', cacherCarte);

// Carte d'information
const info = document.getElementById('info');
const carteInfo = document.getElementById('carteInfo');
const fermerInfo = document.getElementById('fermerInfo');
const carteRegles = document.getElementById('carteRegles');
const fermerRegles = document.getElementById('fermerRegles');

// Fonction pour afficher une question aléatoire dans #maCarte
function afficherInfo() {
  // Afficher la carte avec un effet de fondu
  carteInfo.style.opacity = '0';
  carteInfo.style.display = 'block';
  cache.style.opacity = '0';
  cache.style.display = 'block';

  // Lancer l'animation de fondu
  setTimeout(() => {
    carteInfo.style.opacity = '1';
    cache.style.opacity = '0.5';
  }, 100);
}

function afficherRegles() {
  // Afficher la carte avec un effet de fondu
  carteRegles.style.opacity = '0';
  carteRegles.style.display = 'block';
  cache.style.opacity = '0';
  cache.style.display = 'block';

  // Lancer l'animation de fondu
  setTimeout(() => {
    carteRegles.style.opacity = '1';
    cache.style.opacity = '0.5';
  }, 100);
}

// Fonction pour cacher la carte avec un effet de fondu
function cacherInfo() {
  carteInfo.style.opacity = '1';
  cache.style.opacity = '0.5';

  // Lancer l'animation de fondu
  setTimeout(() => {
    carteInfo.style.opacity = '0';
    carteInfo.style.display = 'none';
    cache.style.opacity = '0';
    cache.style.display = 'none';
  }, 100);
}

function cacherRegles() {
  carteRegles.style.opacity = '1';
  cache.style.opacity = '0.5';

  // Lancer l'animation de fondu
  setTimeout(() => {
    carteRegles.style.opacity = '0';
    carteRegles.style.display = 'none';
    cache.style.opacity = '0';
    cache.style.display = 'none';
  }, 100);
}

// Ajouter un écouteur d'événement au clic sur #cartesBoite
info.addEventListener('click', afficherInfo);
regles.addEventListener('click', afficherRegles);

// Ajouter un écouteur d'événement au clic sur #fermer
fermerInfo.addEventListener('click', cacherInfo);
fermerRegles.addEventListener('click', cacherRegles);

// Plein écran
const fullscreen = document.getElementById('fullscreen');
const maPage = document.documentElement;

function passageFullscreen() {
	if (!window.screenTop && !window.screenY) {
		document.exitFullscreen();
		fullscreen.src = "src/screen-off.svg";
	} else {
		maPage.requestFullscreen();
		fullscreen.src = "src/screen-on.svg";
	}
}

fullscreen.addEventListener('click', passageFullscreen);
afficherRegles();