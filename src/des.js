window.addEventListener('DOMContentLoaded', function() {
  var boutonDes = document.getElementById('boutonDes');
  var monDes = document.getElementById('monDes');

  boutonDes.addEventListener('click', function() {
      lecteur.pause();
      lecteur.src = "src/des.mp3";
      lecteur.currentTime = 0;
      lecteur.play();
    boutonDes.classList.add('clignoter'); // Ajoute la classe "clignoter" pour l'effet de clignotement

    setTimeout(function() {
      var resultatDes = Math.floor(Math.random() * 6) + 1; // Génère un nombre aléatoire entre 1 et 6
      monDes.src = 'src/' + resultatDes + '.svg'; // Met à jour l'image du dé en fonction du résultat
      
      boutonDes.classList.remove('clignoter'); // Supprime la classe "clignoter" pour arrêter l'effet de clignotement
    }, 1000); // Attend 2 secondes avant d'afficher le nombre final
  });
});