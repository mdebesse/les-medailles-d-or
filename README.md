# Les médaillés d’or

Jeu de plateau sur les Jeux Olympiques de Paris 2024 créé par les élèves du dispositif ULIS de Mme Justine Roland de l'école Jules Desplous de Rimogne.

Il a été adapté au format numérique par Morgan Debesse, ERUN de la circonscription de Revin dans les Ardennes.

Il est porposé sous licence CC-BY-SA